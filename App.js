import React, { Component } from 'react'
import { StatusBar, StyleSheet, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Website from './src/screen/Website';
import TorchScreen from './src/screen/TorchScreen';
import Dashboard from './src/screen/Dashboard';
import GoogleSignUpScreen from './src/screen/GoogleSignUpScreen';
import FacebookSignUpScreen from './src/screen/FacebookSignUpScreen';
import VideoPlayerScreen from './src/screen/VideoPlayerScreen';
import DynamicUIScreen from './src/screen/DynamicUIScreen';
import { Provider } from 'react-redux';
import store from './src/redux_store/store';
import GraphScreen from './src/screen/GraphScreen';
import LineChartScreen from './src/chart/LineChartScreen';
import BezierChartScreen from './src/chart/BezierChartScreen';
import ProgressChartScreen from './src/chart/ProgressChartScreen';
import PieChartScreen from './src/chart/PieChartScreen';
import ContributionChartScreen from './src/chart/ContributionChartScreen';
import StackedBarChartScreen from './src/chart/StackedBarChartScreen';
import BarCodeScreen from './src/screen/BarCodeScreen';
import Splash from './src/screen/Splash';
import AudioPlayerScreen from './src/screen/AudioPlayerScreen';
import Console from './src/node/Console';
import NodeScreen from './src/screen/NodeScreen';
import ConnectScreen from './src/mysql/ConnectScreen';

import ViewPagerScreen from './src/screen/ViewPagerScreen';
import MakeCall from './src/screen/MakeCall';

const Stack = createStackNavigator();

class App extends Component {


  render() {
    return (
      <Provider store={store} >
        <View style={styles.container}>
          <StatusBar backgroundColor="#6112e0" barStyle="light-content" />
          {Platform.OS === 'ios' && (
            <StatusBar backgroundColor="red" barStyle="light-content" />
          )}
          <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
              <Stack.Screen name='Splash' component={Splash} />
              <Stack.Screen name='Dashboard' component={Dashboard} />
              <Stack.Screen name='TorchScreen' component={TorchScreen} />
              <Stack.Screen name='Website' component={Website} />
              <Stack.Screen name='BarCodeScreen' component={BarCodeScreen} />
              <Stack.Screen name='GoogleSignUpScreen' component={GoogleSignUpScreen} />
              <Stack.Screen name='FacebookSignUpScreen' component={FacebookSignUpScreen} />
              <Stack.Screen name='VideoPlayerScreen' component={VideoPlayerScreen} />
              <Stack.Screen name='DynamicUIScreen' component={DynamicUIScreen} />

              <Stack.Screen name='GraphScreen' component={GraphScreen} />
              <Stack.Screen name="LineChartScreen" component={LineChartScreen} />
              <Stack.Screen name='BezierChartScreen' component={BezierChartScreen} />
              <Stack.Screen name='ProgressChartScreen' component={ProgressChartScreen} />
              <Stack.Screen name='PieChartScreen' component={PieChartScreen} />
              <Stack.Screen name='ContributionChartScreen' component={ContributionChartScreen} />
              <Stack.Screen name='StackedBarChartScreen' component={StackedBarChartScreen} />

              <Stack.Screen name='AudioPlayerScreen' component={AudioPlayerScreen} />

              {/* Node */}
              <Stack.Screen name='NodeScreen' component={NodeScreen} />
              <Stack.Screen name='Console' component={Console} />
              <Stack.Screen name='ConnectScreen' component={ConnectScreen} />

              <Stack.Screen name='ViewPagerScreen' component={ViewPagerScreen} />
              <Stack.Screen name='MakeCall' component={MakeCall} />

            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'azure',
  },
})


export default App