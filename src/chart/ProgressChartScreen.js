import React, { Component } from 'react'
import { Dimensions, Text, View } from 'react-native'
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";


class ProgressChartScreen extends Component {
  render() {

    const data = {
      labels: ["Swim", "Bike", "Run"], // optional
      data: [0.4, 0.6, 0.8]
    };

    const chartConfig = {
      backgroundGradientFrom: "green",
      backgroundGradientFromOpacity: 0,
      backgroundGradientTo: "green",
      backgroundGradientToOpacity: 0.5,
      color: (opacity = 1) => `rgba(256, 235, 226, ${opacity})`,
      strokeWidth: 2, // optional, default 3
      barPercentage: 0.5,
      useShadowColorFromDataset: false // optional
    };

    return (
      <View>
        <Text>Progress Ring</Text>

        <ProgressChart
          data={data}
          width={screenWidth}
          height={220}
          strokeWidth={16}
          radius={32}
          chartConfig={chartConfig}
          hideLegend={false}
        />

      </View>
    )
  }
}

const screenWidth = Dimensions.get("window").width;

export default ProgressChartScreen;