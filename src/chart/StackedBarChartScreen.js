import React, { Component } from 'react'
import { Dimensions, Text, View } from 'react-native'
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";


class StackedBarChartScreen extends Component {
  render() {

    const data = {
      labels: ["Test1", "Test2"],
      legend: ["L1", "L2", "L3"],
      data: [
        [60, 60, 60],
        [30, 30, 60]
      ],
      barColors: ["#dfe4ea", "#ced6e0", "#a4b0be"]
    };

    const chartConfig = {
      backgroundGradientFrom: "orange",
      backgroundGradientFromOpacity: 0,
      backgroundGradientTo: "green",
      backgroundGradientToOpacity: 0.5,
      color: (opacity = 1) => `rgba(256, 25, 226, ${opacity})`,
      strokeWidth: 2, // optional, default 3
      barPercentage: 0.5,
      useShadowColorFromDataset: false // optional
    };

    const graphStyle = {
      width:300,
      height:300,
    };

    return (
      <View>
        <Text>Stacked Bar Chart</Text>

        <StackedBarChart
          style={graphStyle}
          data={data}
          width={screenWidth}
          height={220}
          chartConfig={chartConfig}
        />

      </View>
    )
  }
}

const screenWidth = Dimensions.get("window").width;

export default StackedBarChartScreen;