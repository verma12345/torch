import React, { Component } from 'react'
import { Dimensions, Text, View } from 'react-native'
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";


class ContributionChartScreen extends Component {
  render() {

    const commitsData = [
      { date: "2017-01-02", count: 1 },
      { date: "2017-01-03", count: 2 },
      { date: "2017-01-04", count: 3 },
      { date: "2017-01-05", count: 4 },
      { date: "2017-01-06", count: 5 },
      { date: "2017-01-30", count: 2 },
      { date: "2017-01-31", count: 3 },
      { date: "2017-03-01", count: 2 },
      { date: "2017-04-02", count: 4 },
      { date: "2017-03-05", count: 2 },
      { date: "2017-02-30", count: 4 }
    ];
    const chartConfig = {
      backgroundGradientFrom: "green",
      backgroundGradientFromOpacity: 0,
      backgroundGradientTo: "green",
      backgroundGradientToOpacity: 0.5,
      color: (opacity = 1) => `rgba(256, 25, 226, ${opacity})`,
      strokeWidth: 2, // optional, default 3
      barPercentage: 0.5,
      useShadowColorFromDataset: false // optional
    };

    return (
      <View>
        <Text>Contribution Graph</Text>

        <ContributionGraph
          values={commitsData}
          endDate={new Date("2017-04-01")}
          numDays={105}
          width={screenWidth}
          height={220}
          chartConfig={chartConfig}
        />
      </View>
    )
  }
}

const screenWidth = Dimensions.get("window").width;

export default ContributionChartScreen;