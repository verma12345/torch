import React, { Component } from 'react'
import { Dimensions, Text, View } from 'react-native'
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";


class BezierChartScreen extends Component {
  render() {

    const data = {
      labels: ["January", "February", "March", "April", "May", "June"],
      datasets: [
        {
          data: [20, 45, 28, 80, 99, 43],
          color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
          strokeWidth: 2 // optional
        }
      ],
      legend: ["Rainy Days"] // optional
    };

    const chartConfig = {
      backgroundGradientFrom: "green",
      backgroundGradientFromOpacity: 0,
      backgroundGradientTo: "green",
      backgroundGradientToOpacity: 0.5,
      color: (opacity = 1) => `rgba(256, 25, 226, ${opacity})`,
      strokeWidth: 2, // optional, default 3
      barPercentage: 0.5,
      useShadowColorFromDataset: false // optional
    };

    return (
      <View>
        <Text>Bezier Line Chart</Text>

        <LineChart
          data={data}
          width={screenWidth}
          height={256}
          verticalLabelRotation={30}
          chartConfig={chartConfig}
          bezier
        />


      </View>
    )
  }
}

const screenWidth = Dimensions.get("window").width;

export default BezierChartScreen;