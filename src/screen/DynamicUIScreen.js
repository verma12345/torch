import React, { Component } from 'react'
import { Dimensions, FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MyInput from '../components/MyInput';

import { setDeleteIssueRow, setEmptyData, setUpdateEstimateAmount, setVisibility } from '../redux_store/actions/indexActions';

class DynamicUIScreen extends Component {
    componentDidMount() {
        this.props.setEmptyData(true);
    }

    _onCrossClick = (index) => {
        if (this.props.data_row.length == 1) {
            alert("You can't delete all Text input.")
        } else {
            this.props.setDeleteIssueRow(index)
        }
    }

    _renderItem = (dataItem) => {
        return (
            <TouchableOpacity style={[styles.listItem, { marginBottom: this.props.data_row.length == dataItem.index + 1 ? 50 : 0 }]}  >
                <Text style={{ fontSize: 18 }} >{dataItem.item.item_name} </Text>
            </TouchableOpacity>
        )
    }
    render() {
        let input = this.props.data_row.map((item, index) => {
            return (
                <MyInput
                    index={index}
                    length={this.props.data_row.length}
                    placeholder="Name..."
                    value={item.item_name}

                    onChangeValue={(value) => {
                        this.props.setUpdateEstimateAmount(value, index);
                    }}

                    onCrossClick={(index) => {
                        this._onCrossClick(index)
                    }}
                />
            )
        })
        console.log(this.props.data_row);
        return (
            <SafeAreaView style={styles.container} >

                <ScrollView>
                    {input}
                </ScrollView>
                <View style={styles.btnViewStyle} >
                    <TouchableOpacity
                        onPress={() => { this.props.setEmptyData(false) }}
                        style={styles.touchStyle} >
                        <Text style={{ fontSize: 22 }} >{"Add new input"} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => { this.props.setVisibility(true) }}
                        style={styles.touchStyle}
                    >
                        <Text style={{ fontSize: 22 }} >{"Show name"} </Text>
                    </TouchableOpacity>
                </View>

                {
                    this.props.isVisible ?
                        <View style={styles.popUp} >
                            <View style={styles.popUpItem} >
                                <FlatList
                                    data={this.props.data_row}
                                    renderItem={this._renderItem}
                                />
                                <TouchableOpacity
                                    style={{ alignItems: "center", elevation: 5, borderTopWidth: 1 }}
                                    onPress={() => this.props.setVisibility(false)}
                                >
                                    <Text style={{ fontSize: 22 }} >{"Ok"}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        :
                        null
                }


            </SafeAreaView>
        )
    }
}

const { width, height } = Dimensions.get("window")

const styles = StyleSheet.create({
    container: {
        width,
        height,
        // flex:1,
    },
    listItem: {
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white",
        elevation: 10,
        padding: 5,
        margin: 5,
        borderRadius:5
    },
    popUp: {
        width,
        height,
        backgroundColor: "#0006",
        position: "absolute"
    },
    popUpItem: {
        backgroundColor: 'white',
        flex: 1,
        marginHorizontal: 50,
        marginVertical: 100,
        borderRadius: 10,
    },
    btnViewStyle: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom: 20,
    },
    touchStyle: {
        alignItems: "center",
        flex: 1,
        justifyContent: "center",
        height: 40,
        elevation: 20,
        backgroundColor: "white"
    }
})

const mapStateToProps = (state) => {
    const common = state.indexReducer;
    return {
        data_row: common.data_row,
        isVisible: common.isVisible,
    };
};

const mapDsipatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setEmptyData: (isEmpty) => setEmptyData(isEmpty),
            setDeleteIssueRow: (index) => setDeleteIssueRow(index),
            setUpdateEstimateAmount: (value, index) => setUpdateEstimateAmount(value, index),
            setVisibility: (isTrue) => setVisibility(isTrue)
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDsipatchToProps)(DynamicUIScreen);
