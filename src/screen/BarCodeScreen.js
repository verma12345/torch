import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
class BarCodeScreen extends Component {
    state = {
        barcodes: [],
    }

    barcodeRecognized = ({ barcodes }) => this.setState({ barcodes });

    barcodeRecognized = ({ barcodes }) => {
        barcodes.forEach(barcode => console.warn(barcode.data))
    };


    renderBarcode = ({ bounds, data }) => (
        <React.Fragment key={data + bounds.origin.x}>
          <View
            style={{
              borderWidth: 2,
              borderRadius: 10,
              position: 'absolute',
              borderColor: '#F00',
              justifyContent: 'center',
              backgroundColor: 'rgba(255, 255, 255, 0.9)',
              padding: 10,
              ...bounds.size,
              left: bounds.origin.x,
              top: bounds.origin.y,
            }}
          >
            <Text style={{
              color: '#F00',
              flex: 1,
              position: 'absolute',
              textAlign: 'center',
              backgroundColor: 'transparent',
            }}>{data}</Text>
          </View>
        </React.Fragment>
      );

    renderBarcodes = () => (
        <View>
            {this.state.barcodes.map(this.renderBarcode)}
        </View>
    );
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>Welcome to React Native!</Text>
                <Text style={styles.instructions}>To get started, edit App.js</Text>
                {/* <Text style={styles.instructions}>{instructions}</Text> */}
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={{
                        flex: 1,
                        width: '100%',
                    }}
                    onGoogleVisionBarcodesDetected={this.barcodeRecognized}
                >
                    {this.renderBarcodes()}
                </RNCamera>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    welcome: {
        fontSize: 18
    },
    instructions: {
        fontSize: 16
    }
})
export default BarCodeScreen;