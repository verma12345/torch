import React, { Component } from 'react'
import { Dimensions, SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import Option from '../components/Option';

class Dashboard extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container} >
                <ScrollView
                    contentContainerStyle={{ flex: 1 }}
                >
                    <View style={[styles.rowStyle, { marginTop: 20 }]} >
                        <Option
                            option_name="Torch"
                            onPress={() => this.props.navigation.navigate("TorchScreen")}
                            icon={require("../../assets/torch.png")}
                        />
                        <Option
                            option_name="Website"
                            onPress={() => this.props.navigation.navigate("Website")}
                            icon={require("../../assets/web.png")}

                        />
                        <Option
                            option_name="Bar Code Scanner"
                            onPress={() => this.props.navigation.navigate("BarCodeScreen")}
                            icon={require("../../assets/barcode.png")}
                        />
                    </View>

                    <View style={styles.rowStyle}>
                        <Option
                            option_name="SignUp with google"
                            onPress={() => this.props.navigation.navigate("GoogleSignUpScreen")}
                            icon={require("../../assets/google.gif")}
                        />
                        <Option
                            option_name="SignUp with Facebook"
                            onPress={() => this.props.navigation.navigate("FacebookSignUpScreen")}
                            icon={require("../../assets/facebook.png")}
                        />

                        <Option
                            option_name="Vieo player"
                            onPress={() => this.props.navigation.navigate("VideoPlayerScreen")}
                            icon={require("../../assets/video.png")}
                        />

                    </View>

                    <View style={styles.rowStyle}>
                        <Option
                            option_name="Dynamic UI"
                            onPress={() => this.props.navigation.navigate("DynamicUIScreen")}
                            icon={require("../../assets/dynamic.png")}
                        />
                        <Option
                            option_name="Graph"
                            onPress={() => this.props.navigation.navigate("GraphScreen")}
                            icon={require("../../assets/graph.png")}
                        />
                        <Option
                            option_name="Audio Payer"
                            onPress={() => this.props.navigation.navigate("AudioPlayerScreen")}
                            icon={require("../../assets/music.png")}
                        />
                    </View>

                    <View style={styles.rowStyle}>
                        <Option
                            option_name="Node.js"
                            onPress={() => this.props.navigation.navigate("NodeScreen")}
                            icon={require("../../assets/node.png")}
                        />

                        <Option
                            option_name="Setting"
                            onPress={() => this.props.navigation.navigate("Setting")}
                            icon={require("../../assets/setting_black.png")}
                        />

                        <Option
                            option_name="View Pager"
                            onPress={() => this.props.navigation.navigate("ViewPagerScreen")}
                            icon={require("../../assets/pager.png")}
                        />
                    </View>

                    <View style={styles.rowStyle}>
                        <Option
                            option_name="Make a call"
                            onPress={() => this.props.navigation.navigate("MakeCall")}
                            icon={require("../../assets/call.webp")}
                        />

                        
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        width,
        height,
        justifyContent: "space-between",
        backgroundColor: '#fafaff',
    },
    rowStyle: {
        flexDirection: "row",
    }
})

export default Dashboard;