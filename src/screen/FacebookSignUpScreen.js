import React from 'react'
import { View } from 'react-native'
import { LoginButton, AccessToken } from 'react-native-fbsdk';

const FacebookSignUpScreen = () => {

    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: 'center' }}>
            <LoginButton
                onLoginFinished={
                    (error, result) => {
                        if (error) {
                            console.log("login has error: " + result.error);
                        } else if (result.isCancelled) {
                            console.log("login is cancelled.");
                        } else {
                            AccessToken.getCurrentAccessToken().then(
                                (data) => {
                                    // console.log(data.accessToken.toString())
                                    console.log(data);
                                }
                            )
                            
                        }
                    }
                }
                onLogoutFinished={() => console.log("logout.")} />
        </View>
    )
}

export default FacebookSignUpScreen;