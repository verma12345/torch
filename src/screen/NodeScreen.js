import React, { Component } from 'react'
import { Dimensions, SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import OptionVertical from '../components/OptionVertical';




class NodeScreen extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container} >
                <ScrollView
                    contentContainerStyle={{ flex: 1 }}
                >
                    <View style={[styles.rowStyle, { marginTop: 20 }]} >
                        <OptionVertical
                            option_name="Console"
                            onPress={() => this.props.navigation.navigate("Console")}
                            icon={require("../../assets/node1.png")}
                        />

                        <OptionVertical
                            option_name="mySql"
                            onPress={() => this.props.navigation.navigate("ConnectScreen")}
                            icon={require("../../assets/node1.png")}
                        />

                    </View>


                </ScrollView>
            </SafeAreaView>
        )
    }
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        width,
        height,
        justifyContent: "space-between",
        backgroundColor: '#fafaff',
    },

    rowStyle: {
        flexDirection: "row",
    }
})

export default NodeScreen;