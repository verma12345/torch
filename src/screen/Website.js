import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import WebView from 'react-native-webview';

class Website extends Component {
    render() {
        return (
            <View style={{ flex: 1 }} >
                <WebView
                    source={{ uri: 'https://reactnative.dev/' }}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    originWhitelist={['*']}
                />
            </View>
        )
    }
}

export default Website
