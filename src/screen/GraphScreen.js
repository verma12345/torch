import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import Header from '../components/Header';
import Option from '../components/Option';

class GraphScreen extends Component {
    render() {
        return (
            <View >
                <Header
                    header="Graph"
                    onPress={() => this.props.navigation.goBack()}
                />

                <View style={[styles.rowStyle, { marginTop: 10 }]}>
                    <Option
                        option_name="Line Chart"
                        onPress={() => this.props.navigation.navigate("LineChartScreen")}
                        icon={require("../../assets/graph.png")}

                    />

                    <Option
                        option_name="Bezier Line Chart"
                        onPress={() => this.props.navigation.navigate("BezierChartScreen")}
                        icon={require("../../assets/bezier.png")}
                    />
                    <Option
                        option_name="Progress Ring Chart"
                        onPress={() => this.props.navigation.navigate("ProgressChartScreen")}
                        icon={require("../../assets/progress.png")}
                    />
                </View>

                <View style={styles.rowStyle}>
                    <Option
                        option_name="Pie Chart"
                        onPress={() => this.props.navigation.navigate("PieChartScreen")}
                        icon={require("../../assets/pie.png")}
                    />

                    <Option
                        option_name="Contribution Chart"
                        onPress={() => this.props.navigation.navigate("ContributionChartScreen")}
                        icon={require("../../assets/contribution.jpeg")}
                    />
                    <Option
                        option_name="Stacked Chart"
                        onPress={() => this.props.navigation.navigate("StackedBarChartScreen")}
                        icon={require("../../assets/stack.png")}
                    />
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    rowStyle: {
        flexDirection: "row",
    }
})

export default GraphScreen;