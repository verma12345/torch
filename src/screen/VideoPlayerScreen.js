import React, { Component } from 'react'
import { StyleSheet, View, ScrollView, Alert, Text, TouchableOpacity, Image, Dimensions, StatusBar } from 'react-native'
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Orientation from 'react-native-orientation';
import Video from 'react-native-video';

class VideoPlayerScreen extends Component {

    videoPlayer;
    state = {
        currentTime: 0,
        duration: 0,
        isFullScreen: false,
        isLoading: false,
        paused: false,
        playerState: PLAYER_STATES.PLAYING,
        screenType: 'contain'
    }

    onSeek = seek => {
        this.videoPlayer.seek(seek);
    }

    onPaused = playerState => {
        this.setState({
            paused: !this.state.paused,
            playerState
        });
    }

    onReplay = () => {
        this.setState({ playerState: PLAYER_STATES.PLAYING });
        this.videoPlayer.seek(0);
    }

    onProgress = data => {
        const { isLoading, playerState } = this.state;
        if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
            this.setState({ currentTime: data.currentTime });
        }
    }

    onLoad = data => {
        this.setState({ duration: data.duration, isLoading: false });
    }

    onLoadStart = data => {
        this.setState({ isLoading: true });
    }

    onEnd = () => {
        this.setState({ playerState: PLAYER_STATES.ENDED });
    }

    onError = () => {
        alert(error)
    }

    exitFullScreen = () => {
        alert('Exit full screen')
    }

    enterFullScreen = () => {
        if (this.state.screenType == 'contain') {
            this.setState({ screenType: 'cover' })
        } else {
            this.setState({ screenType: 'contain' })
        }
    }

    renderToolbar = () => (
        <View>
            <Text style={styles.toolbar}> toolbar </Text>
        </View>
    );

    onSeeking = currentTime => {
        this.setState({ currentTime })
    }

    changeOreantation = () => {
        if( Dimensions.get('window').width < Dimensions.get('window').height ){
            Orientation.lockToLandscape()
        }else{
            Orientation.lockToPortrait()
        }
        
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar hidden />
                <Video
                    onEnd={this.onEnd}
                    onLoad={this.onLoad}
                    onLoadStart={this.onLoadStart}
                    onProgress={this.onProgress}
                    paused={this.state.paused}
                    ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                    resizeMode={this.state.screenType}
                    onFullScreen={this.state.isFullScreen}
                    source={{ uri: 'https://www.radiantmediaplayer.com/media/big-buck-bunny-360p.mp4', }}
                    // source={require("../../assets/video/voice.mp4")}
                    style={styles.mediaPlayer}
                    volume={10}
                />

                <MediaControls
                    duration={this.state.duration}
                    isLoading={this.state.isLoading}
                    mainColor="#332"
                    onFullScreen={this.onFullScreen}
                    onPaused={this.onPaused}
                    onReplay={this.onReplay}
                    onSeek={this.onSeek}
                    onSeeking={this.onSeeking}
                    playerState={this.state.playerState}
                    progress={this.state.currentTime}
                    toolbar={this.renderToolbar}
                />

                <TouchableOpacity
                    onPress={() => {
                        this.changeOreantation()
                    }}
                    style={{ alignItems: 'flex-end',margin:10, }}
                >
                    <Image
                        style={{ width: 30, height: 30 }}
                        source={require("../../assets/full.png")}
                    />

                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-end"
    },

    mediaPlayer: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: 'black',
        justifyContent: 'center',
    },
    toolbar: {
        marginTop: 30,
        backgroundColor: "white",
        padding: 10,
        borderRadius: 5
    }
})
export default VideoPlayerScreen;