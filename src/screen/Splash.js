import React, { Component } from 'react'
import { ActivityIndicator, Text, View } from 'react-native'
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator,
} from 'react-native-indicators';


class Splash extends Component {
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('Dashboard')
        }, 2000)
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", alignItems: "center" }} >

                {/* <ActivityIndicator size="large" color="#6112e0" /> */}
                {/* <DotIndicator color='#6112e0' /> */}
                {/* <BallIndicator color='#6112e0' /> */}
                <BarIndicator count={10} color='#6112e0' />
                {/* <MaterialIndicator count={10} color='#6112e0' /> */}
                {/* <PacmanIndicator count={10} color='#6112e0' /> */}
                {/* <PulseIndicator count={10} color='#6112e0' /> */}
                {/* <SkypeIndicator count={10} color='#6112e0' /> */}
                {/* <UIActivityIndicator size={100}  count={10} color='#6112e0' /> */}
                {/* <WaveIndicator size={100} count={10} color='#6112e0' /> */}

            </View>
        )
    }
}
export default Splash;