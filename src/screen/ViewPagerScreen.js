import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { MyPager } from '../components/MyPager'

class ViewPagerScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <MyPager />
            </View>
        )
    }
}
export default ViewPagerScreen;