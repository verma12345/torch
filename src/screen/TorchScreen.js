import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Torch from 'react-native-torch'

class TorchScreen extends Component {

  onTorch = () =>{
    Torch.switchState(true)
  }

  offTorch = () =>{
    Torch.switchState(false)
  }

  render() {
    return (
      <View>
        <Text style={styles.header}>
          {'Simple Torch'}
        </Text>

        <TouchableOpacity
        onPress={()=>this.onTorch()}
          style={styles.onStyle}
        >
          <Text style={styles.onText} >
            {'On'}
          </Text>
        </TouchableOpacity>


        <TouchableOpacity
        onPress={()=>this.offTorch()}
          style={styles.offStyle}
        >
          <Text style={styles.offText} >
            {'Off'}
          </Text>
        </TouchableOpacity>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    alignSelf: "center",
    margin: 20,
    fontSize: 22,
    color: "black",
    fontWeight: 'bold'
  },
  onStyle: {
    alignSelf: "center",
    paddingVertical:20,
  },
  offStyle: {
    alignSelf: "center",
    paddingVertical:20,
  },
  onText:{
    color:'green',
    fontWeight:'bold',
    fontSize:18
  },
  offText:{
    color:"red",
    fontWeight:'bold',
    fontSize:18
  }
})

export default TorchScreen
