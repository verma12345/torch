import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import ViewPager from '@react-native-community/viewpager';

export const MyPager = () => {
    return (
        <ViewPager
          
            style={styles.viewPager} initialPage={0}>
            <View
                style={{ flex: 1, backgroundColor: 'cyan', alignItems: "center", justifyContent: 'center' }}
                key="1">
                <Text>First page</Text>
            </View>
            <View
                style={{ flex: 1, backgroundColor: 'magenta', alignItems: "center", justifyContent: 'center' }}
                key="2">
                <Text>Second page</Text>
            </View>
            <View
                style={{ flex: 1, backgroundColor: 'skyblue', alignItems: "center", justifyContent: 'center' }}
                key="2">
                <Text>Second page</Text>
            </View>
            
        </ViewPager>
    );
};

const styles = StyleSheet.create({
    viewPager: {
        flex: 1,
    },
});