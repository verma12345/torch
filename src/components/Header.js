import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const Header = (props) => {
    return (
        <View style={[styles.container, styles.myshadow]} >
            <View
                style={{
                    height: 40,
                    width: 40,
                    alignItems: "center",
                    justifyContent: "center"
                }}
                onPress={props.onPress}
            >
                <Image
                    style={{ height: 25, width: 35 }}
                    source={require("../../assets/back_black.png")}
                />
            </View>
            <Text style={{ color: "white", fontSize: 22, marginEnd: 150 }} >
                {props.header}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 40,
        backgroundColor: "#6112e0",
        elevation: 5,
        alignItems: 'baseline',
        justifyContent: "space-between",
        flexDirection: "row",

    },
    myshadow: {
        shadowColor: "#1050e6",
        shadowOpacity: 0.15,

        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
    },
    txtStyle: {
        fontSize: 16,
        fontWeight: "bold",
        textAlign: "center",
        alignSelf: 'center'
    }
})

export default Header;