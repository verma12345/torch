import React from 'react'
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';

const BottomNav = (props) => {
    return (
        <View style={styles.container} >

            <TouchableOpacity
                style={styles.leftStyle}
            >
            </TouchableOpacity>

            <View
                style={{
                    marginHorizontal: -5,
                    flex: 0.3, elevation: 10, backgroundColor: '#fafaff',
                }}
            >
                <View style={{
                    backgroundColor: '#fafaff',
                    flex: 1,
                    borderRadius: 30,
                    marginTop: -30,
                    marginBottom: -30,
                    marginLeft: -2,
                    marginRight: -1,
                    elevation: 2,
                    justifyContent: "center",


                }} >
                    <View style={{ flex: 1,margin:5, borderRadius: 30,alignItems:"center" }} >
                        <Image
                        style={{width:35,height:35}}
                        source={require("../../assets/avatar.png")}
                        />
                    </View>
                </View>
                <View style={{ backgroundColor: 'white', flex: 1 }} >

                </View>
            </View>

            <TouchableOpacity
                style={styles.rightStyle}
            >
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 40,
        backgroundColor: '#fafaff',
        flexDirection: "row"
    },
    leftStyle: {
        flex: 1,
        backgroundColor: "white",
        elevation: 10,
        borderTopRightRadius: 20
    },

    rightStyle: {
        flex: 1,
        backgroundColor: "white",
        elevation: 10,
        borderTopLeftRadius: 20
    }
})
export default BottomNav;