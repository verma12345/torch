import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { getWidth } from './Layout'

const Option = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.container, styles.myshadow]} >
            <Image
                source={props.icon}
                style={{ height: 30, width: 30 }}
            />
            <Text style={styles.txtStyle} >
                {props.option_name}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: getWidth(110),
        height: getWidth(110),
        backgroundColor: "white",
        borderRadius: 10,
        justifyContent: "center",
        padding: 2,
        margin: 5,
        alignItems: 'center'
    },
    myshadow: {
        shadowColor: "#1050e6",
        shadowOpacity: 0.15,

        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
    },
    txtStyle: {
        fontSize: 16,
        fontWeight: "bold",
        textAlign: "center",
        alignSelf: 'center'
    }
})

export default Option;