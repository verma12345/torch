import React from 'react'
import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

const MyInput = (props) => {
    return (
        <View
            style={[styles.inputStyle, styles.myshadow, { marginBottom: props.length == props.index + 1 ? 50 : 0, }]}
        >
            <TextInput
                placeholder={props.placeholder}
                onChangeText={(value) => {
                    props.onChangeValue(value, props.index)
                }}
                style={{flex:1}}
            />

            <TouchableOpacity
                onPress={() => {
                    props.onCrossClick(props.index)
                }}
                style={{
                    elevation: 10,
                    borderRadius: 30,
                }}
            >
                <Image
                    style={{ width: 25, height: 25, }}
                    source={require("../../assets/delete.png")}
                />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    inputStyle: {
        height: 50,
        backgroundColor: "white",
        padding: 5,
        margin: 10,
        borderRadius: 5,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    myshadow: {
        // shadowColor: "#1050e6",
        shadowColor: 'blue',
        shadowOpacity: 0.15,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
    },
})
export default MyInput;