export const MODULE_KEY = 'module_app_store_';

export const SET_DATA_ROW = MODULE_KEY + 'SET_DATA_ROW';
export const SET_VISIBILITY = MODULE_KEY + 'SET_VISIBILITY';

