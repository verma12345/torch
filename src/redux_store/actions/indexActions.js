import {
  SET_DATA_ROW, SET_VISIBILITY,
} from './types';
import store from '../store';

export const setEmptyData = (isEmpty) => (dispatch) => {
  let state = store.getState().indexReducer;
  let empty_data = { item_name: '' };
  let data = state.data_row;
  if (isEmpty) {
    data = [];
  }
  let neData = [...data, empty_data];
  dispatch({
    type: SET_DATA_ROW,
    payload: neData,
  });
};

// Update appointment list 
export const setDeleteIssueRow = (index) => (dispatch) => {
  let state = store.getState().indexReducer;
  let data = [...state.data_row];
  data.splice(index, 1);

  dispatch({
    type: SET_DATA_ROW,
    payload: data
  });
};

export const setUpdateEstimateAmount = (value, ind,) => (dispatch) => {
  let state = store.getState().indexReducer;
  let temp = state.data_row;

  let newData = temp.map((item, index) => {
    if (index === ind) {
      return {
        ...item,  // copy the existing item
        item_name: value // replace the email addr  //amount to price 
      }
    }
    return item;
  })
  dispatch({
    type: SET_DATA_ROW,
    payload: newData,
  });
};


export const setVisibility = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_VISIBILITY,
    payload: isTrue,
  });
};

// export const setItemsToArray = (name, id) => (dispatch) => {
//   let state = store.getState().indexReducer;
//   let data_row = state.data_row;
//   data_row.push({
//     name: name,
//     id: id,
//   });
//   dispatch({
//     type: SET_ITEMS_TO_ARRAY,
//     payload: data_row,
//   });
// };

// export const removeItems = (id) => (dispatch) => {
//   let state = store.getState().indexReducer;
//   let data_row = state.data_row;
//   data_row = data_row.filter((item) => item.id !== id);
//   dispatch({
//     type: REMOVE_ITEM,
//     payload: data_row,
//   });
// };

// export const editItems = (name, id) => (dispatch) => {
//   let state = store.getState().indexReducer;
//   let data_row = [];
//   state.data_row.map((item) => {
//     if (item.id == id) {
//       data_row.push({
//         name: name,
//         id: item.id,
//       });
//     } else {
//       data_row.push({
//         name: item.name,
//         id: item.id,
//       });
//     }
//   });
//   dispatch({
//     type: EDIT_ITEM,
//     payload: data_row,
//   });
// };


// export const searchUpdate = () => (dispatch) => {
//   let state = store.getState().indexReducer;
//   let txt = state.inputText
//   let holder = state.data_row;
//   let newData = holder.filter(function (item) {
//     return item.name.includes(txt);
//   });
//   dispatch({
//     type: SEARCH_UPDATE,
//     payload: newData,
//   });
// };



// export const hitAPI = search_text => dispatch => {
//   const search = search_text.trim();
//   if (search === '') return;
//   dispatch({type: SET_LOADING, payload: true});

//   let url = `https://api.flickr.com/services/rest/?method=flickr.photos.search&text=${search}&api_key=cb8e65679bb25463361e9da0faacad58&per_page=20&format=json&nojsoncallback=1&page=1`;

//   fetch(url, {
//     method: 'GET',
//   })
//     .then(response => response.json())
//     .then(responseJson => {
//       // //responseJson);
//       if (responseJson.stat == 'ok') {
//         // console.log(responseJson.thumb_url);
//         // // let markers = [];
//         let data = responseJson.photos.photo;
//         dispatch({type: ON_DATA_CHANGE, payload: data});
//         dispatch({type: SET_LOADING, payload: false});
//       } else {
//         alert('Zero result found');
//       }
//     })
//     .catch(error => {
//       console.error(error);
//     });
// };
