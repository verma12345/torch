import {
 SET_DATA_ROW, SET_VISIBILITY,
} from '../actions/types';

const initialState = {
  data_row:[],
  isVisible:false,
  featured_brand:[
    {
      image:require("../../../assets/iphone.jpeg")
    },
    {
      image:require("../../../assets/galaxy.jpg")
    },
    {
      image:require("../../../assets/realme.jpg")
    },
    {
      image:require("../../../assets/iphone.jpeg")
    },
    {
      image:require("../../../assets/galaxy.jpg")
    },

  ]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_DATA_ROW:
      return {
        ...state,
        data_row: action.payload,
      };

      case SET_VISIBILITY:
        return {
          ...state,
          isVisible: action.payload,
        };

    default:
      return state;
  }
}
