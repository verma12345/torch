import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
class Console extends Component {

    onLog = () => {
        console.log("This is log");
    }

    onError = () => {
        console.error("This is error");
    }

    onWarn = () => {
        console.warn("This is warning");
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }} >
                <TouchableOpacity
                    onPress={() => this.onLog()}
                    style={styles.option} >
                    <Text>
                        {'Log'}
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => this.onError()}
                    style={{ backgroundColor: "white", width: 200, alignItems: "center", elevation: 5, marginVertical: 5, paddingHorizontal: 30, paddingVertical: 5 }} >
                    <Text>
                        {'Error'}
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => this.onWarn()}
                    style={{ backgroundColor: "white", width: 200, alignItems: "center", elevation: 5, marginVertical: 5, paddingHorizontal: 30, paddingVertical: 5 }} >
                    <Text>
                        {'Warn'}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: 50,
    },
    option: {
        backgroundColor: "white",
        width: 200,
        alignItems: "center",
        elevation: 5,
        marginVertical: 5,
        paddingHorizontal: 30,
        paddingVertical: 5
    }
})
export default Console;